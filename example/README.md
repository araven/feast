////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  The  FEAST Eigenvalue Solver     ///////////////////////////
//////////// ///////////////////////////////////////////////////////////////////////////////
web   : http://www.feast-solver.org  
E-mail: feastsolver@gmail.com              
/////////////////////////////////////////////////////////////////////////////////////////////


FEAST EXAMPLE:     -Selected examples for Fortran, C, Fortran-MPI, C-MPI 
                   -Two directories are provided: Hermitian and Non-Hermitian
                   -The examples are solved using the different feast_dense, feast_banded, and feast_sparse drivers.


*********************************************************************************************


- See Instructions in the FEAST documentation section "FEAST Applications"


- The examples deal with four systems: system1 (a real symmetric generalized eigenvalue problem), 
                                       system2 (a complex Hermitian standard eigenvalue problem),
                                       system3 (a real non-symmetric  generalized eigenvalue problem),
                                       system4 (a complex symmetric  standard eigenvalue problem),




                     

